COMPUTER=AMPERE

NVCC=nvcc
CFLAGS+=-O2 -g
LFLAGS_INTEL=-lcusolver -lcublas -lm -lcurand -lgomp -lpthread -ldl
LFLAGS_AMD=-lcusolver -lcublas -lm -lcurand -lgomp -lpthread -ldl
LFLAGS=

ifeq ($(COMPUTER), AMPERE)
CFLAGS+=-DAMPERE
LFLAGS+=$(LFLAGS_INTEL)
endif
ifeq ($(COMPUTER), TINKER_CLIFFS)
CFLAGS+=-DTINKER_CLIFFS
LFLAGS+=$(LFLAGS_AMD) -lopenblas
endif
ifeq ($(COMPUTER), CU12_MKL)
LFLAGS+=$(LFLAGS_INTEL) -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core
endif

OBJS=demo.o dict.o dmd.o sketch.o utils.o

all: demo

demo: $(OBJS)
	$(NVCC) $(CFLAGS) $^ -o $@ $(LFLAGS)

%.o: %.cu
	$(NVCC) $(CFLAGS) -c $^ -o $@

clean:
	rm -f $(OBJS)
