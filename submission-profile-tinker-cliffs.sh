#!/bin/bash
#SBATCH -J gpu-dmd-self-time
#SBATCH --account=personal
#SBATCH --partition=a100_normal_q
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=1
#SBATCH --gres=gpu:1 --mem=128G
#SBATCH --time=0:10:00

date
echo "-----------------------------------"
module load cuda-latest/blas
module load cuda-latest/toolkit
module load cuda-latest/nsight
module load openblas/dynamic
CFLAGS=-DPROFILE
make clean
make
ncu -o prof-tka100.%i.out.ncu-rep --section ComputeWorkloadAnalysis --section InstructionStats --section LaunchStats --section MemoryWorkloadAnalysis --section Occupancy --section SchedulerStats --section SpeedOfLight ./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 1
#./demo
echo "-----------------------------------"
date
