#pragma once

#include "utils.h"

class AbstractGpuMatrix {
public:
	virtual int getM();
	virtual int getN();
	virtual void toMatrix(matrix_t *out); // allocates out

	// like utils.h matrix_mult
	// if (side=='L'), do A*B, else if (side=='R'), do B*A
	// B must be on the GPU
	virtual void matmul(matrix_t *out, matrix_t *B, char side='L');

	// apply sequence of householder transformations in the strictly lower triangular part of Q
	// out should already be allocated
	// dimensions of out and this matrix determine truncation
	// if (trans=='N'), apply left to right, else if (trans='T'), apply right to left
	// Q must be on the GPU
	virtual void applyHHseq(matrix_t *out, matrix_t *Q, char trans='N');
};
