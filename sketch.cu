#include "sketch.h"
#include <curand.h>

#define V2_SUM_LEN 10
#define V3_SUM_LEN 8

__global__ void matrix_sketch_k2(float *out, int m, int n, int mylen, float *seed) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = (threadIdx.y + blockIdx.y*blockDim.y) * mylen;
	if (i < m && j < n) {
		float x = seed[i*mylen + j];
		for (int k=0; j+k<n; k++) {
			float s = 0;
			for (int l=0; l<V2_SUM_LEN; l++) {
				x = 4*x*(1-x);
				s += x;
			}
			out[i + (j+k)*m] = s;
		}
	}
}

__global__ void matrix_sketch_k3(float *out, int m, int n, int mylen, float *seed) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = (threadIdx.y + blockIdx.y*blockDim.y) * mylen;
	if (i < m && j < n) {
		float x = 1 - seed[i*mylen + j]*seed[i*mylen + j];
		for (int k=0; k<mylen && j+k<n; k++) {
			float s = 0;
			for (int l=0; l<V3_SUM_LEN; l++) {
				x = x*x - 0.5f;
				s += x;
			}
			out[i + (j+k)*m] = s;
		}
	}
}

static float *c_logistic_seed; // in (0,1)
static int logistic_seed_len;

__global__ void matrix_sketch_init_seed_k1(float *seed, int len) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if (i < len) {
		float x = (i+1) / (float) (len+2);
		for (int j=0; j<V2_SUM_LEN; j++) {
			x = 4*x*(1-x);
		}
		seed[i] = x;
	}
}

__global__ void matrix_sketch_advance_seed_k1(float *seed, int len) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if (i < len) {
		float x = seed[i];
		for (int j=0; j<V2_SUM_LEN; j++) {
			x = 4*x*(1-x);
		}
		seed[i] = x;
	}
}

static void reseed(int new_len) {
	if (new_len > logistic_seed_len) {
		if (c_logistic_seed) {
			cudaFree(c_logistic_seed);
		}
		cudaMalloc(&c_logistic_seed, sizeof(float)*new_len);
		logistic_seed_len = new_len;
		matrix_sketch_init_seed_k1 <<< DIV_UP(new_len,256), 256 >>> (c_logistic_seed, new_len);
	} else {
		matrix_sketch_advance_seed_k1 <<< DIV_UP(new_len,256), 256 >>> (c_logistic_seed, new_len);
	}
}

void matrix_sketch(matrix_t *out, int ver) {
	printf("Sketching %p with method %d\n", out, ver);
	curandGenerator_t cugen;
	dim3 B(DIV_UP(out->m,16), DIV_UP(out->n,16*16));
	dim3 T(16, 16);
	if (ver < 1) ver = 1;
	if (ver > 3) ver = 3;
	switch (ver) {
	case 1:
		curandCreateGenerator(&cugen, CURAND_RNG_PSEUDO_XORWOW);
		curandSetPseudoRandomGeneratorSeed(cugen, 12345);
		curandGenerateNormal(cugen, out->data, out->m*out->n, 0, 1.0/out->n);
		break;
	case 2:
		reseed(out->m*out->n);
		matrix_sketch_k2 <<< B, T >>> (out->data, out->m, out->n, 16, c_logistic_seed);
		break;
	case 3:
		reseed(out->m*out->n);
		matrix_sketch_k3 <<< B, T >>> (out->data, out->m, out->n, 16, c_logistic_seed);
		printf("%d,%d\n", T.x, T.y);
		printf("%d,%d\n", B.x, B.y);
		{ CHECK_FOR_ERRORS; }
		break;
	}
}
