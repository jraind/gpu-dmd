static int *c_info = NULL;

#include <assert.h>
#include <cuda.h>
#include "dmd.h"
#include "sketch.h"

#define MIN(a,b) (((a)<(b)) ? (a) : (b))

// out = A^T/D = (D\A)^T
// strange profiling results (can probably optimise), but average runtime less than launch latency
__global__ void dmd_trans_and_div_diag_k1(float *out, float *A, float *D, int m, int n)
{
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < m && j < n) {
		out[j+i*n] = A[i+j*m] / D[i];
	}
}

// assumes cusolver already initialised
static void gpu_qr(matrix_t *c_X, matrix_t *c_T, cusolverDnParams_t params) {
	matrix_t T = { NULL, MIN(c_X->m,c_X->n), 1, 1, 1 };
	*c_T = T;
	matrix_alloc(c_T);
	size_t c_buf_size;
	size_t h_buf_size;
#ifndef PROFILE
	CUSOLVER_CHECK(cusolverDnXgeqrf_bufferSize(cusolver_h, params,
		c_X->m, c_X->n, CUDA_R_32F, c_X->data, c_X->m,
		CUDA_R_32F, c_T->data,
		CUDA_R_32F,
		&c_buf_size, &h_buf_size));
	void *c_buf;
	void *h_buf;
	cudaMalloc(&c_buf, c_buf_size);
	h_buf = malloc(h_buf_size);
	TIME("Compute QR decomposition: ",
	CUSOLVER_CHECK(cusolverDnXgeqrf(cusolver_h, params,
		c_X->m, c_X->n, CUDA_R_32F, c_X->data, c_X->m,
		CUDA_R_32F, c_T->data,
		CUDA_R_32F,
		c_buf, c_buf_size, h_buf, h_buf_size, c_info));
	);
	cudaFree(c_buf);
	free(h_buf);
#endif
}

__global__ void dmd_copy_part_k1(float *out, float *in, int ldo, int ldi, int n) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < ldo && i < ldi && j < n) {
		out[i+j*ldo] = in[i+j*ldi];
	}
}

static void gpu_q_mult(matrix_t *c_Y, matrix_t *c_A, matrix_t *c_T, matrix_t *c_X) {
	TIME("Multiply by QR Q factor: ",
	int c_buf_size;
	*c_Y = *c_X;
	c_Y->data = NULL;
	c_Y->m = c_A->m;
	matrix_alloc(c_Y);
	cudaMemset(c_Y->data, 0, c_Y->m*c_Y->n*sizeof(float));
#ifndef PROFILE
	CUSOLVER_CHECK(cusolverDnSormqr_bufferSize(cusolver_h, CUBLAS_SIDE_LEFT, CUBLAS_OP_N,
		c_X->m, c_X->n, c_A->n,
		c_A->data, c_A->m,
		c_T->data,
		c_Y->data, c_Y->m,
		&c_buf_size));
	c_buf_size *= 10;
	float *c_buf;
	cudaMalloc(&c_buf, c_buf_size);
	CUSOLVER_CHECK(cusolverDnSormqr(cusolver_h, CUBLAS_SIDE_LEFT, CUBLAS_OP_N,
		c_X->m, c_X->n, c_A->n,
		c_A->data, c_A->m,
		c_T->data,
		c_Y->data, c_Y->m,
		c_buf, c_buf_size, c_info));
	cudaFree(c_buf);
#endif
	);
}

static void gpu_qt_mult(matrix_t *c_Y, matrix_t *c_A, matrix_t *c_T, matrix_t *c_X) {
	TIME("Multiply by QR Q^T factor: ",
	int c_buf_size;
	c_Y->data = 0;
	matrix_alloc(c_Y);
	cudaMemset(c_Y->data, 0, c_Y->m*c_Y->n*sizeof(float));
#ifndef PROFILE
	CUSOLVER_CHECK(cusolverDnSormqr_bufferSize(cusolver_h, CUBLAS_SIDE_LEFT, CUBLAS_OP_T,
		c_A->m, c_X->n, c_A->n,
		c_A->data, c_A->m,
		c_T->data,
		c_Y->data, c_Y->m,
		&c_buf_size));
	c_buf_size *= 10;
	float *c_buf;
	cudaMalloc(&c_buf, c_buf_size);
	CUSOLVER_CHECK(cusolverDnSormqr(cusolver_h, CUBLAS_SIDE_LEFT, CUBLAS_OP_T,
		c_A->m, c_X->n, c_A->n,
		c_A->data, c_A->m,
		c_T->data,
		c_Y->data, c_Y->m,
		c_buf, c_buf_size, c_info));
	cudaFree(c_buf);
#endif
	);
}

static void gpu_svd(matrix_t *c_U, matrix_t *c_S, matrix_t *c_Vt, matrix_t *X, int alg, dmd_opts_t *opts) {
	ensure_cusolver();

	if (alg < 1) alg = 1;
	if (alg > 4) alg = 4;

	matrix_t c_X;
	int need_to_free_c_X = 1;
	if (X->gpu && X->colmaj) {
		c_X = *X;
		need_to_free_c_X = 0;
	} else if (X->gpu) {
		matrix_t tplt = { NULL, 0, 0, 1, 1 };
		matrix_copy(&c_X, X, &tplt);
	} else if (X->colmaj) {
		matrix_upload(&c_X, X);
	} else {
		matrix_t h_Xt;
		matrix_trans(&h_Xt, X);
		matrix_upload(&c_X, &h_Xt);
	}

	int k;
	switch (alg) {
	case 1:
		k = (X->m < X->n) ? X->m : X->n;
		break;
	case 2:
	case 3:
		k = opts->rank;
		break;
	case 4:
		k = opts->rank + (opts->p ? opts->p : 5);
		break;
	}

	c_U->m = X->m;
	c_U->n = c_S->m = c_Vt->m = X->n;
	c_S->n = 1;
	c_Vt->n = X->n;
	c_U->data = c_S->data = c_Vt->data = NULL;
	c_U->colmaj = c_S->colmaj = c_Vt->colmaj = 1;
	c_U->gpu = c_S->gpu = c_Vt->gpu = 1;
	matrix_alloc(c_U);
	matrix_alloc(c_S);
	matrix_alloc(c_Vt);

	size_t h_buf_size, c_buf_size;
	cusolverDnParams_t params;
	cusolverDnCreateParams(&params);
	switch (alg) {
	case 1:
		cusolverDnXgesvdp_bufferSize(cusolver_h, params, CUSOLVER_EIG_MODE_VECTOR, 1,
				X->m, X->n, CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->m,
				CUDA_R_32F,
				&c_buf_size, &h_buf_size);
		break;
	case 2:
#ifndef TINKER_CLIFFS
		cusolverDnXgesvdr_bufferSize(cusolver_h, params, 'S', 'S',
				X->m, X->n, k, MIN(X->m-k, MIN(X->n-k, k)), 0,
				CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->m,
				CUDA_R_32F,
				&c_buf_size, &h_buf_size);
#endif
		break;
	case 3:
#ifndef TINKER_CLIFFS
		cusolverDnXgesvdr_bufferSize(cusolver_h, params, 'S', 'S',
				X->m, X->n, k, (opts->p ? opts->p : 5), 0,
				CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->m,
				CUDA_R_32F,
				&c_buf_size, &h_buf_size);
		break;
#endif
	case 4:
		cusolverDnXgesvdp_bufferSize(cusolver_h, params, CUSOLVER_EIG_MODE_VECTOR, 1,
				k, X->n, CUDA_R_32F, c_X.data, X->m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, X->m,
				CUDA_R_32F, c_Vt->data, c_Vt->n,
				CUDA_R_32F,
				&c_buf_size, &h_buf_size);
		break;
	}
	void *h_buf = malloc(h_buf_size);
	void *c_buf;
	cudaMalloc(&c_buf, c_buf_size);

	double *h_err_sigma;
	switch (alg) {
	case 1:
		h_err_sigma = (double*) malloc(X->m * sizeof(double));
#ifndef PROFILE
		CUSOLVER_CHECK(cusolverDnXgesvdp(cusolver_h, params, CUSOLVER_EIG_MODE_VECTOR, 1,
				X->m, X->n, CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->m,
				CUDA_R_32F,
				c_buf, c_buf_size, h_buf, h_buf_size,
				c_info, h_err_sigma));
		free(h_err_sigma);
#endif
		break;
	case 2:
		c_U->n = c_S->m = c_Vt->m = k;
		c_Vt->colmaj = 0;
#ifndef TINKER_CLIFFS
#ifndef PROFILE
		CUSOLVER_CHECK(cusolverDnXgesvdr(cusolver_h, params, 'S', 'S',
				X->m, X->n, k, MIN(X->m-k, MIN(X->n-k, k)), 2,
				CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->n,
				CUDA_R_32F,
				c_buf, c_buf_size, h_buf, h_buf_size, c_info));
#endif
		break;
	case 3:
		c_U->n = c_S->m = c_Vt->m = k;
		c_Vt->colmaj = 0;
#ifndef PROFILE
		CUSOLVER_CHECK(cusolverDnXgesvdr(cusolver_h, params, 'S', 'S',
				X->m, X->n, k, (opts->p ? opts->p : 5), 2,
				CUDA_R_32F, c_X.data, c_X.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_U->data, c_U->m,
				CUDA_R_32F, c_Vt->data, c_Vt->n,
				CUDA_R_32F,
				c_buf, c_buf_size, h_buf, h_buf_size, c_info));
#endif
#endif
		break;
	case 4:
		{
		size_t free_mem, total_mem;
		CUDA_CHECK(cudaMemGetInfo(&free_mem, &total_mem));
		printf("free: %lu, total: %lu\n", free_mem, total_mem);
		c_U->n = c_S->m = c_Vt->m = k;
		matrix_t sketchmat = { NULL, X->n, k, 1, 1 };
		matrix_alloc(&sketchmat);
		matrix_sketch(&sketchmat, opts->sketchver);
		matrix_t XOmega = { NULL, X->m, k, 1, 1 };
		matrix_alloc(&XOmega);
		matrix_mult(&XOmega, &c_X, &sketchmat);
		matrix_t c_T;
		gpu_qr(&XOmega, &c_T, params);
		matrix_t c_B = { NULL, XOmega.m, X->n, 1, 1 };
		gpu_qt_mult(&c_B, &XOmega, &c_T, &c_X);
		matrix_t c_Ub = { NULL, XOmega.m, k, 1, 1 };
		matrix_alloc(&c_Ub);
		cudaMemset(c_Ub.data, 0, c_Ub.m*c_Ub.n*sizeof(float));
		h_err_sigma = (double*) malloc(c_B.m * sizeof(double));
#ifndef PROFILE
		TIME("Form dense economy SVD: ", CUSOLVER_CHECK(cusolverDnXgesvdp(cusolver_h, params,
				CUSOLVER_EIG_MODE_VECTOR, 1,
				k, c_B.n, CUDA_R_32F, c_B.data, c_B.m,
				CUDA_R_32F, c_S->data,
				CUDA_R_32F, c_Ub.data, c_Ub.m,
				CUDA_R_32F, c_Vt->data, c_Vt->n, // TODO: transpose
				CUDA_R_32F,
				c_buf, c_buf_size, h_buf, h_buf_size,
				c_info, h_err_sigma)););
#endif
		free(h_err_sigma);
		matrix_free(c_U);
		gpu_q_mult(c_U, &XOmega, &c_T, &c_Ub);
		matrix_free(&sketchmat);
		matrix_free(&XOmega);
		matrix_free(&c_Ub);
		matrix_free(&c_T);
		matrix_free(&c_B);
		break;
		}
	}

	cudaFree(c_buf);
	free(h_buf);
	if (need_to_free_c_X) {
		matrix_free(&c_X);
	}
}

void dmd(matrix_t *Fl, matrix_t *Fr, matrix_t *X, matrix_t *Y, float rtol, int alg, dmd_opts_t *opts) {
	assert(X->m == Y->m);
	assert(X->n == Y->n);
	if (c_info == NULL) cudaMalloc(&c_info, sizeof(int));
	assert(c_info);
	matrix_t c_Y;
	if (Y->gpu) {
		c_Y = *Y;
	} else {
		matrix_upload(&c_Y, Y);
	}

	matrix_t c_U, c_Sg, c_Vt;
	gpu_svd(&c_U, &c_Sg, &c_Vt, X, alg, opts);
	matrix_t h_S;
	matrix_download(&h_S, &c_Sg);
	int rank = 1;
	for (int i=1; i<h_S.m; i++) {
		if (matrix_get_index(&h_S, i, 0) / matrix_get_index(&h_S, 0, 0) >= rtol) {
			rank = i + 1;
		}
	}
	if (rank >= h_S.m) {
		rank = h_S.m - 1;
	}
	matrix_free(&h_S);
	matrix_slice_t c_Ur = { &c_U, 0, 0, c_U.m, rank };
	matrix_slice_t c_Sr = { &c_Sg, 0, 0, rank, 1 };
	matrix_slice_t c_Vtr = { &c_Vt, 0, 0, rank, c_Vt.n };

	matrix_t c_VSi = *c_Vtr.base;
	c_VSi.m = c_Vt.n;
	c_VSi.n = rank;
	c_VSi.data = NULL;
	matrix_alloc(&c_VSi);
	dim3 B(DIV_UP(c_VSi.m,16), DIV_UP(c_VSi.n,16));
	dim3 T(16, 16);
	dmd_trans_and_div_diag_k1 <<< B, T >>> (c_VSi.data, c_Vtr.base->data, c_Sr.base->data, c_Vtr.m, c_Vtr.n);
	matrix_t c_Br = { NULL, Y->m, c_VSi.n, 1, 1 };
	matrix_alloc(&c_Br);
	matrix_mult(&c_Br, &c_Y, &c_VSi);
	matrix_free(&c_VSi);
	matrix_t c_S = { NULL, c_Br.n, c_Br.n, 1, 1 };
	matrix_alloc(&c_S);
	matrix_t c_Ut = { c_U.data, c_U.n, c_U.m, !c_U.colmaj, 1 };
	matrix_slice_t c_Urt = { &c_Ut, 0, 0, c_Ur.n, c_Ur.m };
	matrix_mult(&c_S, &c_Urt, &c_Br);
	matrix_free(&c_Br);

	matrix_copy(Fl, &c_Ur, NULL);
	assert(Fl->gpu);
	*Fr = *Fl;
	Fr->m = c_S.m;
	Fr->n = c_Urt.n;
	Fr->data = NULL;
	matrix_alloc(Fr);
	assert(Fr->gpu);
	matrix_mult(Fr, &c_S, &c_Urt);

	matrix_free(&c_S);
	matrix_free(&c_U);
	matrix_free(&c_Sg);
	matrix_free(&c_Vt);
	if (!Y->gpu) {
		matrix_free(&c_Y);
	}
}
