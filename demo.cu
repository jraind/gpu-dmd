#include <assert.h>
#include <cuda.h>
#include "dict.h"
#include "dmd.h"
#include "utils.h"

float time_dmd(matrix_t *X, matrix_t *Y, dict_t dict, int ord, int alg, dmd_opts_t *opts, int trials) {
	assert(X->gpu);
	assert(Y->gpu);

	cudaEvent_t start, stop;
	CUDA_CHECK(cudaEventCreate(&start));
	CUDA_CHECK(cudaEventCreate(&stop));

	CUDA_CHECK(cudaEventRecord(start));
	for (int i=0; i<trials; i++) {
		matrix_t PhiX, PhiY;
		dict_eval(&PhiX, X, dict, ord);
		dict_eval(&PhiY, Y, dict, ord);
		matrix_t Fl, Fr;
		dmd(&Fl, &Fr, &PhiX, &PhiY, 3e-2, alg, opts);
		matrix_free(&PhiX);
		matrix_free(&PhiY);
		matrix_free(&Fl);
		matrix_free(&Fr);
	}
	CUDA_CHECK(cudaEventRecord(stop));

	CUDA_CHECK(cudaEventSynchronize(stop));
	float ms = 0;
	CUDA_CHECK(cudaEventElapsedTime(&ms, start, stop));
	return ms / 1000 / trials;
}

void dmd_benchmark(matrix_t *timings, matrix_t *X, matrix_t *Y, dict_t dict, int trials) {
#ifndef PROFILE
	int algs[] = { 1, 2, 3, 4, 4, 4 };
	dmd_opts_t opts[] = { {0}, {100}, {100}, { 100, 0, 1 }, { 100, 0, 2 }, { 100, 0, 3 } };
	int ords[] = { 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 16, 18, 20, 24, 28, 34, 40, 48, 60 };
#else
//	int algs[] = { 3, 4, 4 };
//	dmd_opts_t opts[] = { {20}, { 20, 0, 1 }, { 20, 0, 3 } };
	int algs[] = { 4 };
	dmd_opts_t opts[] = { { 20, 0, 3 } };
	int ords[] = { 3 };
#endif
	timings->m = sizeof(ords) / sizeof(int);
	timings->n = sizeof(algs) / sizeof(int);
	timings->colmaj = 0;
	timings->gpu = 0;
	timings->data = NULL;
	matrix_alloc(timings);
	for (int i=0; i<timings->n; i++) {
		time_dmd(X, Y, dict, 2, algs[i], opts+i, 1);
		for (int j=0; j<timings->m; j++) {
			*matrix_index(timings, j, i) = time_dmd(X, Y, dict, ords[j], algs[i], opts+i, trials);
		}
	}
}

int main(int argc, char **argv) {
	ensure_cusolver(); // this should not *need* to be here, but it does
	ensure_cublas();
	assert(argc == 4 || argc == 5);
	matrix_t timings, h_X, h_Y;
	matrix_load(&h_X, argv[1]);
	matrix_load(&h_Y, argv[2]);
	matrix_t c_X, c_Y;
	matrix_upload(&c_X, &h_X);
	matrix_upload(&c_Y, &h_Y);
	matrix_free(&h_X);
	matrix_free(&h_Y);
	dmd_benchmark(&timings, &c_X, &c_Y, DICT_LAGUERRE_NOCROSS, argc>=5?atoi(argv[4]):1);
	matrix_save(&timings, argv[3]);
	matrix_free(&c_X);
	matrix_free(&c_Y);
	matrix_free(&timings);
	return 0;
}
