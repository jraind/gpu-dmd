#pragma once

#include <cusolverDn.h>
#include <stdio.h>

#define DIV_UP(a, b) (((a)+(b)-1) / (b))
#define ROUND_UP(a, b) (DIV_UP(a,b) * (b))

#define CUDA_CHECK(e) do { \
	cudaError CUDA_CHECK_status = (e); \
	if (CUDA_CHECK_status != cudaSuccess) { \
		fprintf(stderr, "CUDA call " #e " at %s:%d failed with %d\n", __FILE__, __LINE__, CUDA_CHECK_status); \
		printf("last cuda error: %s\n", cudaGetErrorName(cudaPeekAtLastError())); \
		abort(); \
	} } while (0)

#define CUBLAS_CHECK(e) do { \
	cublasStatus_t CUBLAS_CHECK_status = (e); \
	if (CUBLAS_CHECK_status != CUBLAS_STATUS_SUCCESS) { \
		fprintf(stderr, "cuBlas call " #e " at %s:%d failed with %d\n", __FILE__, __LINE__, CUBLAS_CHECK_status); \
		printf("last cuda error: %s\n", cudaGetErrorName(cudaPeekAtLastError())); \
		abort(); \
	} } while (0)

#define CUSOLVER_CHECK(e) do { \
	cusolverStatus_t CUSOLVER_CHECK_status = (e); \
	if (CUSOLVER_CHECK_status != CUSOLVER_STATUS_SUCCESS) { \
		fprintf(stderr, "cuSolver call " #e " at %s:%d failed with %d\n", __FILE__, __LINE__, CUSOLVER_CHECK_status); \
		printf("last cuda error: %s\n", cudaGetErrorName(cudaPeekAtLastError())); \
		abort(); \
	} } while (0)

#define CHECK_FOR_ERRORS do { \
	cudaError CHECK_FOR_ERRORS_status = cudaGetLastError(); \
	if (CHECK_FOR_ERRORS_status != cudaSuccess) { \
		fprintf(stderr, "last cuda error: %s\n", cudaGetErrorName(CHECK_FOR_ERRORS_status)); \
	} \
	} while(0)

#define TIME(fmt, code) do { \
	cudaEvent_t TIME_start, TIME_stop; \
	cudaEventCreate(&TIME_start); \
	cudaEventCreate(&TIME_stop); \
	cudaEventRecord(TIME_start); \
	code \
	cudaEventRecord(TIME_stop); \
	cudaEventSynchronize(TIME_stop); \
	float TIME_ms = 0; \
	cudaEventElapsedTime(&TIME_ms, TIME_start, TIME_stop); \
	printf(fmt " %g microseconds\n", TIME_ms*1000); \
	} while (0)

#define cudaMalloc(a, b) CUDA_CHECK(cudaMalloc(a, b))
#define cudaFree(a) CUDA_CHECK(cudaFree(a))
#define cudaMemcpy(a, b, c, d) CUDA_CHECK(cudaMemcpy(a, b, c, d))
#define cudaMemset(a, b, c) CUDA_CHECK(cudaMemset(a, b, c))

extern cublasHandle_t cublas_h;
extern cusolverDnHandle_t cusolver_h;

// call before calling cublas and cusolver, respectively
void ensure_cublas();
void ensure_cusolver();

typedef struct matrix_t_struct {
	float *data;
	int m, n;
	unsigned colmaj : 1;
	unsigned gpu : 1;
} matrix_t;

typedef struct matrix_slice_t_struct {
	matrix_t *base;
	int i0, j0;
	int m, n;
} matrix_slice_t;

// allocate data for a matrix
// this is ment to be used for initialisation as follows:
//  1: declare or allocate a matrix
//  2: zero out the matrix (initialising with { NULL } works)
//  3: set the following parameters as desired:
//    * m      is the height of the matrix
//    * n      is the width of the matrix
//    * colmaj is 1 iff the matrix is column major
//    * gpu    is 1 iff the matrix is on the gpu
void matrix_alloc(matrix_t *mat);

// deallocate data for a matrix
void matrix_free(matrix_t *mat);

// send a matrix to the GPU
// copies parameters (other than gpu) and data to out
// out may or may not not be allocated
void matrix_upload(matrix_t *out, matrix_t *in);
void matrix_upload(matrix_t *out, matrix_slice_t *in);

// copy a matrix from the GPU
// copies parameters (other than gpu) and data to out
// out may or may not be allocated
void matrix_download(matrix_t *out, matrix_t *in);
void matrix_download(matrix_t *out, matrix_slice_t *in);

// get a pointer to the ijth entry of the matrix
// crashes if gpu is 1
float *matrix_index(matrix_t *mat, int i, int j);
float *matrix_index(matrix_slice_t *mat, int i, int j);

// get the data from the ijth entry of a matrix (even if it is on the gpu)
float matrix_get_index(matrix_t *mat, int i, int j);
float matrix_get_index(matrix_slice_t *mat, int i, int j);

// copy a matrix's data and parameters
// if tplt != NULL, use parameters (except n & m) from tplt (short for template)
// out should not be allocated
void matrix_copy(matrix_t *out, matrix_t *in, matrix_t *tplt);
void matrix_copy(matrix_t *out, matrix_slice_t *in, matrix_t *tplt);

// lazily transpose a matrix (without copying data)
void matrix_trans(matrix_t *out, matrix_t *in);

// multiply matrices A*B together
// out should already be allocated
void matrix_mult(matrix_t *out, matrix_t *A, matrix_t *B);
void matrix_mult(matrix_t *out, matrix_slice_t *A, matrix_t *B);
void matrix_mult(matrix_t *out, matrix_t *A, matrix_slice_t *B);
void matrix_mult(matrix_t *out, matrix_slice_t *A, matrix_slice_t *B);

// out = A^T A for triangular A
// uplo is 'U' or 'R' if A is upper triangular, 'L' for lower triangular
// diag is 'N' for diagonal of A, 'U' for unit diagonal, 'Z' for zero diagonal
// out should already be allocated
void matrix_trsyrk(matrix_t *out, matrix_t *A, char uplo, char diag);

// load a matrix from a named file
// the folliwing formats are supported, determined by header
//  * matrix market coordinate format
// out should not be allocated
void matrix_load(matrix_t *out, const char *filename);

// save a matrix to a named file
// uses matrix market coordinate format
// out should not be allocated
void matrix_save(matrix_t *mat, const char *filename);

// calculate the sum of products of scalars associated with edges for increasingly ordered paths times a scalar
// \sum_{\{(j_1,\dots,j_N) : j_1,\dots,j_N \in [l,r], \{\iota_1,\dots,\iota_I\} \subseteq \{j_1,\dots,j_N\}, \{\nu_1,\dots,\nu_X\} \cap \{j_1,\dots,j_N\} = \emptyset, j_i < j_{i+1}\}}
//     \alpha \prod_{\ell=1}^{N-1} s_{j_\ell,j_{\ell+1}}
// N is a variable ranging from I to r-l+1-#\{i : \nu_i \in [l,r]\}
// N - #\{i : \iota_i \in [l,r]\} must be less than 64
//     if this is about applying householder reflectors, N >= 48 will produce a value smaller than machine epsilon anyways
// work has sizeof(int) * n bytes in it
// \{\iota_1,\dots,\iota_I\} and \{\nu_1,\dots,\nu_X\} are represented as bit arrays as follows, described for the \iota s:
//     the length of the bit array is ROUND_UP(n, 64) in bits, or DIV_UP(n, 64) in longs
//     the ith bit of the array is in the i/64th long, at the i%64th bit of said long
//     i \in \{\iota_1,\dots,\iota_I\} if and only if the ith bit is set
// note that the \nu set effectively contains all integers in [0,l) and (r,n] not in the \iota set
// this is intended to be called from a kernel which handles cross thread reductions itself
// it can either be given a full thread of work, or the fixed part can be fixed to different values for multiple calls per thread
// the latter may improve asymptotic complexity in some situations, as this function does not cache partial products
// this is currently optimised under the assumption that most entries are fixed
// the name is an initialism for
// Partial Sum of Products of Scaled Matrix Entries along PATHs
__device__ float utils_pspsmepath_c(float *S, int n, unsigned long *iota, unsigned long *nu, int l, int r, void *work, float alpha);

// like utils_pspspsmepath_c except that X = 0 and \{\iota_1,\dots,\iota_I\} \cap [l,r] = \emptyset
// thus, the non-fixed entries of \{j_1,\dots,j_N\} are contiguous
// this removes the need for a work array
__device__ float utils_pspsmepath_cont_c(float *S, int n, unsigned long *iota, int l, int r, float alpha);

// do the structure independent parts of applying householder reflectors from the QR decomposition
// there is an implicit input matrix X (we want Q X or Q^T X)
// takes in a matrix U (only the strict lower triangular part is accessed) with the householder vectors with unit diagonal
// takes in a vector tau of the norms of said vectors
// takes in a vector y which is U^T X
// takes in a parameter trans; if trans=='N', find Q X, if trans=='T', find Q^T X
// applies matrix_trsyrk to compute intermediates
// applies utils_pspsmepath_c to correct for error in y due to U not being orthogonal
// batch scales U by the corrected y to get Q X (or Q^T X) - X
// the result is stored in R, which is already allocated
// everything must live on the GPU
void matrix_apply_hh_inter(matrix_t *R, matrix_t *U, matrix_t *tau, matrix_t *y, char trans);
