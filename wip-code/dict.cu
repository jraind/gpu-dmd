#include <assert.h>
#include "dict.h"

ImplicitLaguerreNocross::ImlicitLaguerreNocross(matrix_t *base_mat, int order) {
	matrix_t tplt = { NULL, 0, 0, 1, 1 };
	matrix_copy(&base, base_mat, &tplt);
	d = base_mat->m;
	n = base_mat->n;
	ord = order;
	assert(ord >= 2);
}

int ImplicitLaguerreNocross::getM() {
	return d*ord + 1;
}

int ImplicitLaguerreNocross::getN() {
	return n;
}

void ImplicitLaguerreNocross::toMatrix(matrix_t *out) {
	dict_eval(out, &base, DICT_LAGUERRE_NOCROSS, ord);
}

/* TODO: I don't think this matters
#define BLOCK_BASE_ROWS 4
#define BLOCK_BASE_COLS 4
#define BLOCK_B_ROWS 32

__global__ void ImplicitLaguerreNocross_matmul_k1(float *out, float *base, float *B, int d, int ord, int n, int mb) {
	int bi = threadIdx.x;
	int bj = threadIdx.y;
	int bk = threadIdx.z;
	int i = threadIdx.y + blockIdx.y*blockDim.y; // row of base
	int j = threadIdx.x + blockIdx.x*blockDim.x; // row of B i.e. column of base i.e. column of A
	int k = threadIdx.z + blockIdx.z*blockDim.z; // column of B
	__shared__ float bCache[BLOCK_BASE_COLS * BLOCK_B_ROWS];
	if (bi == 0) {
		bCache[bj+bk*BLOCK_BASE_ROWS] = B[j+k*mb];
	}
	float x = base[i+j*d];
	float acc = bCache;
	float ll = 1;

void ImplicitLaguerreNocross::matmul(matrix_t *out, matrix_t *B, char side='L') {
	assert(side == 'L');
	assert(out->m == getM());
	assert(out->n == B->n);
	assert(n == B->m);
	cudaMemset(out->data, 0, out->m*out->n*sizeof(float));
}
*/

void ImplicitLaguerreNocross::matmul(matrix_t *out, matrix_t *B, char side='L') {
	assert(side == 'L');
	assert(out->m == getM());
	assert(out->n == B->n);
	assert(n == B->m);
	matrix_t tmp;
	toMatrix(&tmp);
	matrix_mult(out, &tmp, B);
}

// needs out to be zeroed n dimensional vector
__global__ void ImplicitLaguerreNocross_applyHHsequence_batch_dot_k1(float *out, float *u, float *base, int d, int ord, int m, int n, int mu) {
	int i = threadIdx.x + blockIdx.x*blockDim.x; // row in base
	int j = threadIdx.y + blockIdx.y*blockDim.y; // column in base
	if (i < d && j < n) {
		float x = base[i+j*d];
		float acc = u[d*ord]; // consider cacheing
		float ll = 1;
		float l = 1.0f - x;
		acc += l * u[i*ord]; // strongly consider cacheing
		for (int k=1; k<ord-1; k++) {
			float s = ((2*k+1-x)*l - k*ll) / (k+1);
			acc += s* u[i*ord+k]; // cache to coalesce
			ll = l;
			l = s;
		}
		atomicAdd(out+j, acc);
	}
}

__global__ void ImplicitLaguerreNocross_applyHHsequence_gesyrl_k1(float *out, float *u, float *base, int d, int ord

#define T_X 16
#define T_Y 16
#define T_DIM ((T_X)*(T_Y))

// paralellise over input rows in x and columns in y
// ordered so the constant is the last entry and otherwise its x_1^1 x_1^2 ... x_1^ord x_2^1 x_2^2 and so on
// assumes ord >= 2
// 964 microseconds (roughly)
__global__ void dict_eval_laguerre_nocross_k1(float *out, float *in, int m, int n, int ord, int transIn, int M) {
	long i = threadIdx.x + blockIdx.x*blockDim.x;
	long j = threadIdx.y + blockIdx.y*blockDim.y;
	long K = m*ord + 1;
	if (i == 0 && j < n) {
		out[K-1 + j*M] = 1;
	}
	if (i < m && j < n) {
		float x = transIn ? in[i + j*m] : in[i*n + j];
		out[i*ord + j*M] = 1 - x; // 11% of excessive L2 writes
		out[i*ord + 1 + j*M] = (1.5f-0.5f*x) * out[i*ord + j*M] - 0.5f; // 11% of excessive L2 writes
		for (int k=2; k<ord-1; k++) {
			// this line naïvely has AI about 5/12 ~ 1/3
			out[i*ord + k + j*M] = ((2*k+1-x) * out[i*ord+k-1+j*M] - k * out[i*ord+k-2+j*M]) / (k+1); // 78% of excessive L2 writes
		}
	}
}

// paralellise over input rows in x and columns in y
// ordered so the constant is the last entry and otherwise its block 1 x block 1 x^2 ... block 1 x^ord block 2 x ...
// assumes ord >= 2
// 57 microseconds
__global__ void dict_eval_laguerre_nocross_k2(float *out, float *in, int m, int n, int ord, int transIn, int M) {
	long i = threadIdx.x + blockIdx.x*blockDim.x;
	long it = threadIdx.x;
	long ib = blockIdx.x*blockDim.x*ord;
	long j = threadIdx.y + blockIdx.y*blockDim.y;
	long K = m*ord + 1;
	if (i == 0 && j < n) {
		out[K-1 + j*M] = 1;
	}
	if (i < m && j < n) {
		float x = transIn ? in[i + j*m] : in[i*n + j];
		out[it+ib + j*M] = 1 - x;
		out[it+ib + 1 + j*M] = (1.5f-0.5f*x) * out[i*ord + j*M] - 0.5f;
		for (int k=2; k<ord-1; k++) {
			out[it+ib + k + j*M] = ((2*k+1-x) * out[it*ib+k-1+j*M] - k * out[it+ib+k-2+j*M]) / (k+1);
		}
	}
}

// try to only read from global memory once
// paralellise over input rows in x and columns in y
// ordered so the constant is the last entry and otherwise its block 1 x block 1 x^2 ... block 1 x^ord block 2 x ...
// assumes ord >= 2
// 48 microseconds
__global__ void dict_eval_laguerre_nocross_k3(float *out, float *in, int m, int n, int ord, int transIn, int M) {
	long i = threadIdx.x + blockIdx.x*blockDim.x;
	long it = threadIdx.x;
	long ib = blockIdx.x*blockDim.x*ord;
	long j = threadIdx.y + blockIdx.y*blockDim.y;
	long K = m*ord + 1;
	if (i == 0 && j < n) {
		out[K-1 + j*M] = 1;
	}
	if (i < m && j < n) {
		float x = transIn ? in[i + j*m] : in[i*n + j];
		float pkm2 = out[it+ib + j*M] = 1 - x;
		float pkm1 = out[it+ib + 1 + j*M] = (1.5f-0.5f*x) * (1-x) - 0.5f;
		for (int k=2; k<ord-1; k++) {
			float tmp = pkm1;
			// AI is naïvely 6/4 = 3/2
			pkm1 = out[it+ib + k + j*M] = ((2*k+1-x) * pkm1 - k * pkm2) / (k+1);
			pkm2 = tmp;
		}
	}
}

__global__ void dict_eval_zero_bottom_k1(float *out, int m, int n, int M) {
	int i = threadIdx.x + blockIdx.x*blockDim.x + m;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < M && j < n) {
		out[i + j*M] = 0;
	}
}

void dict_eval_laguerre_nocross(float *out, float *in, int m, int n, int ord, int transIn) {
	long K = m*ord + 1;
	for (long j=0; j<n; j++) {
		out[K-1 + j*K] = 1;
		for (long i=0; i<m; i++) {
			float x = transIn ? in[i + j*m] : in[i*n + j];
			out[i*ord + j*K] = 1 - x;
			out[i*ord + 1 + j*K] = (1.5-0.5*x) * out[i*ord + j*K] - 0.5;
			for (int k=2; k<ord-1; k++) {
				out[i*ord + k + j*K] = ((2*k+1-x) * out[i*ord+k-1+j*K] - k * out[i*ord+k-2+j*K]) / (k+1);
			}
		}
	}
}

void dict_eval(matrix_t *out, matrix_t *in, dict_t dict, int ord) {
	out->n = in->n;
	out->colmaj = 1;
	out->gpu = in->gpu;
	out->data = NULL;
	if (dict == DICT_LAGUERRE_NOCROSS) {
		if (in->gpu) {
			out->m = ROUND_UP(in->m * ord + 1, 32); // https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses, two-dimensional arrays
		} else {
			out->m = in->m * ord + 1;
		}
		matrix_alloc(out);
		if (in->gpu) {
			dim3 B(DIV_UP(out->m-in->m,T_X/4), DIV_UP(in->n,T_Y*4));
			dim3 T(T_X/4,T_Y*4);
			dict_eval_zero_bottom_k1 <<< B, T >>> (out->data, in->m*ord+1, in->n, out->m);
		}
		if (in->gpu && in->n < 32) {
			dim3 B(DIV_UP(in->m,T_DIM), in->n);
			dim3 T(T_DIM, 1);
			dict_eval_laguerre_nocross_k3 <<< B, T >>> (out->data, in->data, in->m, in->n, ord, !in->colmaj, out->m);
		} else if (in->gpu) {
			dim3 B(DIV_UP(in->m,T_DIM), DIV_UP(in->n,4));
			dim3 T(T_X, T_Y);
			dict_eval_laguerre_nocross_k3 <<< B, T >>> (out->data, in->data, in->m, in->n, ord, !in->colmaj, out->m);
		} else {
			dict_eval_laguerre_nocross(out->data, in->data, in->m, in->n, ord, !in->colmaj);
		}
	} else {
		assert(0);
	}
}
