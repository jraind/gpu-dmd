#include <assert.h>
#include <cblas.h>
#include <cuda.h>
#include <math.h>
#include "utils.h"

#define LEAD_DIM(x) ((x)->colmaj ? ((x)->m) : ((x)->n))

cublasHandle_t cublas_h;
cusolverDnHandle_t cusolver_h;

const static float val_one = 1;
const static float val_zero = 0;
const static float *h_one = &val_one;
const static float *h_zero = &val_zero;

void ensure_cublas() {
	static int have_cublas;
	if (!have_cublas) {
		assert(cublasCreate(&cublas_h) == CUBLAS_STATUS_SUCCESS);
		have_cublas = 1;
	}
}

void ensure_cusolver() {
	static int have_cusolver;
	if (!have_cusolver) {
		int major, minor, patch;
		cusolverGetProperty(MAJOR_VERSION, &major);
		cusolverGetProperty(MINOR_VERSION, &minor);
		cusolverGetProperty(PATCH_LEVEL, &patch);
		printf("CUSOLVER Version (Major,Minor,PatchLevel): %d.%d.%d\n", major,minor,patch);
		size_t free, total;
		CUDA_CHECK(cudaMemGetInfo(&free, &total));
		printf("free: %lu, total: %lu\n", free, total);
		CUSOLVER_CHECK(cusolverDnCreate(&cusolver_h));
		have_cusolver = 1;
	}
}

__global__ void matrix_copy_slice_k1(float *out, float *in, int m, int n, int i0, int j0, int ldin) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < m && j < n) {
		out[j*m+i] = in[(j+j0)*ldin+i+i0];
	}
}

// strange profiling results (can probably optimise), but average runtime on order of launch latency
__global__ void matrix_copy_trans_outplace_k1(float *out, float *in, int m, int n, int i0, int j0, int ldin) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < m && j < n) {
		out[i*n+j] = in[(j+j0)*ldin+i+i0];
	}
}

// impossible?
__global__ void matrix_copy_trans_inplace_k1(float *out, int m, int n) {
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	int j = threadIdx.y + blockIdx.y*blockDim.y;
	if (i < m && j < n) {
//		out[i*n+j] = out[j*m+i];
		out[i*n+j] = NAN; // give up and hopefully break in an obvious way
	}
}

void matrix_alloc(matrix_t *out) {
	assert(out->data == NULL);
	if (out->gpu) {
		cudaMalloc(&out->data, out->n*out->m*sizeof(float));
	} else {
		out->data = (float *) malloc(out->n*out->m*sizeof(float));
	}
}

void matrix_free(matrix_t *out) {
	if (out->gpu) {
		cudaFree(out->data);
	} else {
		free(out->data);
	}
	out->data = NULL;
}

static void matrix_copy_internal(matrix_t *out, matrix_slice_t *in, matrix_t *tplt, int allocated) {
	if (!allocated || out->m != in->m || out->n != in->n) {
		if (allocated) {
			if (out->gpu) {
				cudaFree(out->data);
			} else {
				free(out->data);
			}
		}
		if (tplt) {
			*out = *tplt;
			out->n = in->n;
			out->m = in->m;
		} else {
			*out = *in->base;
		}
		out->data = NULL;
		matrix_alloc(out);
	}

	int full = in->m == in->base->m && in->n == in->base->n;
	if (out->gpu) {
		if (out->colmaj == in->base->colmaj && in->base->gpu && full) {
			cudaMemcpy(out->data, in->base->data, in->n*in->m*sizeof(float), cudaMemcpyDeviceToDevice);
		} else if (out->colmaj == in->base->colmaj && full) {
			cudaMemcpy(out->data, in->base->data, in->n*in->m*sizeof(float), cudaMemcpyHostToDevice);
		} else if (in->base->gpu) {
			int n = in->n;
			int m = in->m;
			dim3 B(DIV_UP(m,16), DIV_UP(n,16));
			dim3 T(16, 16);
			matrix_copy_trans_outplace_k1 <<< B, T >>> (out->data, in->base->data, in->m, in->n, in->i0, in->j0, LEAD_DIM(in->base));
		} else if (out->colmaj == in->base->colmaj && in->base->gpu) {
			int n = in->n;
			int m = in->m;
			dim3 B(DIV_UP(m,16), DIV_UP(n,16));
			dim3 T(16, 16);
			matrix_copy_slice_k1 <<< B, T >>> (out->data, in->base->data, in->m, in->n, in->i0, in->j0, LEAD_DIM(in->base));
		} else {
			assert(0); // difficult :(
		}
	} else {
		if (out->colmaj == in->base->colmaj && !in->base->gpu && full) {
			memcpy(out->data, in->base->data, in->n*in->m*sizeof(float));
		} else if (!in->base->gpu) {
			for (int j=0; j<in->n; j++) {
				for (int i=0; i<in->m; i++) {
					*matrix_index(out, i, j) = matrix_get_index(in, i, j);
				}
			}
		} else if (out->colmaj == in->base->colmaj && full) {
			cudaMemcpy(out->data, in->base->data, in->n*in->m*sizeof(float), cudaMemcpyDeviceToHost);
		} else {
			assert(0);
		}
	}
}

void matrix_copy(matrix_t *out, matrix_t *in, matrix_t *tplt) {
	matrix_slice_t in_slice = { in, 0, 0, in->m, in->n };
	matrix_copy_internal(out, &in_slice, tplt, 0);
}
void matrix_copy(matrix_t *out, matrix_slice_t *in, matrix_t *tplt) {
	matrix_copy_internal(out, in, tplt, 0);
}

void matrix_upload(matrix_t *out, matrix_t *in) {
	assert(!in->gpu);
	matrix_t tplt = *in;
	tplt.gpu = 1;
	matrix_copy(out, in, &tplt);
}
void matrix_upload(matrix_t *out, matrix_slice_t *in) {
	assert(!in->base->gpu);
	matrix_t tplt = *in->base;
	tplt.gpu = 1;
	matrix_copy(out, in, &tplt);
}

void matrix_download(matrix_t *out, matrix_t *in) {
	assert(in->gpu);
	matrix_t tplt = *in;
	tplt.gpu = 0;
	matrix_copy(out, in, &tplt);
}
void matrix_download(matrix_t *out, matrix_slice_t *in) {
	assert(in->base->gpu);
	matrix_t tplt = *in->base;
	tplt.gpu = 0;
	matrix_copy(out, in, &tplt);
}

void matrix_trans(matrix_t *out, matrix_t *in) {
	*out = *in;
	out->colmaj = !in->colmaj;
	out->n = in->m;
	out->m = in->n;
}

static float *matrix_index_internal(matrix_t *mat, int i, int j) {
	assert(i >= 0);
	assert(j >= 0);
	assert(i < mat->m);
	assert(j < mat->n);
	if (mat->colmaj) {
		return mat->data + i + j*mat->m;
	} else {
		return mat->data + i*mat->n + j;
	}
}
static float *matrix_index_internal(matrix_slice_t *mat, int i, int j) {
	assert(i >= 0);
	assert(j >= 0);
	assert(i < mat->m);
	assert(j < mat->n);
	return matrix_index_internal(mat->base, i+mat->i0, j+mat->j0);
}

float *matrix_index(matrix_t *mat, int i, int j) {
	assert(!mat->gpu);
	return matrix_index_internal(mat, i, j);
}
float *matrix_index(matrix_slice_t *mat, int i, int j) {
	assert(!mat->base->gpu);
	return matrix_index_internal(mat, i, j);
}

float matrix_get_index(matrix_t *mat, int i, int j) {
	float *ptr = matrix_index_internal(mat, i, j);
	if (mat->gpu) {
		float r;
		cudaMemcpy(&r, ptr, sizeof(float), cudaMemcpyDeviceToHost);
		return r;
	} else {
		return *ptr;
	}
}
float matrix_get_index(matrix_slice_t *mat, int i, int j) {
	float *ptr = matrix_index_internal(mat, i, j);
	if (mat->base->gpu) {
		float r;
		cudaMemcpy(&r, ptr, sizeof(float), cudaMemcpyDeviceToHost);
		return r;
	} else {
		return *ptr;
	}
}

#define COLMAJ_TO_CUBLAS_OP(x) ((x)->colmaj ? CUBLAS_OP_N : CUBLAS_OP_T)
#define TRANS_CUBLAS_OP(x) ((x) == CUBLAS_OP_N ? CUBLAS_OP_T : CUBLAS_OP_N)
#define TRANS_LEAD_DIM(x) ((x)->colmaj ? ((x)->n) : ((x)->m))
#define MATS_TO_CBLAS_OP(x,y) ((x)->colmaj==(y)->colmaj ? CblasNoTrans : CblasTrans)

void matrix_mult(matrix_t *out, matrix_slice_t *A, matrix_slice_t *B) {
	assert(out->m == A->m);
	assert(A->n == B->m);
	assert(out->n == B->n);

	// base cases
	if (out->gpu && A->base->gpu && B->base->gpu) {
		TIME("Matrix multiplication: ", 
		cudaMemset(out->data, 0, out->n*out->m*sizeof(float));
		if (out->colmaj) {
#ifndef PROFILE
			CUBLAS_CHECK(cublasSgemm(cublas_h, COLMAJ_TO_CUBLAS_OP(A->base), COLMAJ_TO_CUBLAS_OP(B->base),
					out->m, out->n, A->n,
					h_one, matrix_index_internal(A,0,0), LEAD_DIM(A->base), matrix_index_internal(B,0,0), LEAD_DIM(B->base),
					h_zero, out->data, LEAD_DIM(out)));
#endif
		} else {
#ifndef PROFILE
			CUBLAS_CHECK(cublasSgemm(cublas_h,
					TRANS_CUBLAS_OP(COLMAJ_TO_CUBLAS_OP(A->base)),
					TRANS_CUBLAS_OP(COLMAJ_TO_CUBLAS_OP(B->base)),
					out->n, out->m, A->n,
					h_one, matrix_index_internal(A,0,0), LEAD_DIM(A->base), matrix_index_internal(B,0,0), LEAD_DIM(B->base),
					h_zero, out->data, TRANS_LEAD_DIM(out)));
#endif
		}
		printf("C%s = A%s B%s\n", out->colmaj?"":"^T", A->base->colmaj?"":"^T", B->base->colmaj?"":"^T");
		printf("%p (%dx%d,%d) = %p (%dx%d,%d) * %p (%dx%d,%d)\n",
				out->data, out->m, out->n, LEAD_DIM(out),
				matrix_index_internal(A,0,0), out->m, A->n, LEAD_DIM(A->base),
				matrix_index_internal(B,0,0), A->n, out->n, LEAD_DIM(B->base));
		);
		return;
	} else if (!out->gpu && !A->base->gpu && !B->base->gpu) {
		memset(out->data, 0, out->n*out->m*sizeof(float));
		cblas_sgemm(out->colmaj ? CblasColMajor : CblasRowMajor,
				MATS_TO_CBLAS_OP(out, A->base), MATS_TO_CBLAS_OP(out, B->base),
				out->m, out->n, A->n,
				1, matrix_index(A,0,0), LEAD_DIM(A->base), matrix_index(B,0,0), LEAD_DIM(B->base),
				0, out->data, LEAD_DIM(out));
		return;
	}

	// figure out where to do the computation
	// uses a model that only cares about cudaMemcpy and floating point operatons
	// i.e. it assumes launch delay and memory access are negligeable
	const double pcie_speed = 242e9; // for PCIe-x16 (B/s)
	const double gpu_speed = 15.62e12; // for RTX-4070 mobile (FLOPS)
	const double cpu_speed = 114e9; // for i9-13900HX (FLOPS)
	// first, figure out how much needs to be sent over PCIe
	long data_on_cpu = 0;
	long data_on_gpu = 0;
	matrix_t *mats[3] = { out, A->base, B->base };
	for (int i=0; i < sizeof(mats)/sizeof(matrix_t*); i++) {
		long data = mats[i]->m * mats[i]->n;
		if (mats[i]->gpu) {
			data_on_gpu += data;
		} else {
			data_on_cpu += data;
		}
	}
	// second, figure out how many FLOPs need to be done (naïvely)
	double work = out->m * (double) (2*A->n-1) * out->n;
	// third, figure out how much time it takes on CPU and GPU
	double cpu_time = data_on_gpu/pcie_speed/4 + cpu_speed*work;
	double gpu_time = data_on_cpu/pcie_speed/4 + gpu_speed*work;
	// fourth, finally decide
	int use_gpu = gpu_time < cpu_time;

	// get to the base cases by uploading or downloading
	if (use_gpu) {
		matrix_t my_out;
		matrix_slice_t my_A = *A;
		matrix_slice_t my_B = *B;
		matrix_t my_A_mat, my_B_mat;
		if (out->gpu) {
			my_out = *out;
		} else {
			matrix_upload(&my_out, out);
		}
		if (A->base->gpu) {
			my_A = *A;
		} else {
			matrix_upload(&my_A_mat, A);
			A->base = &my_A_mat;
		}
		if (B->base->gpu) {
			my_B = *B;
		} else {
			matrix_upload(&my_B_mat, B);
			B->base = &my_B_mat;
		}
		matrix_mult(&my_out, &my_A, &my_B);
		if (out->data != my_out.data) { // did we need to upload out
			matrix_slice_t out_slice = { &my_out, 0, 0, out->m, out->n };
			matrix_copy_internal(out, &out_slice, NULL, 1);
			matrix_free(&my_out);
		}
		if (!A->base->gpu) {
			matrix_free(&my_A_mat);
		}
		if (!B->base->gpu) {
			matrix_free(&my_B_mat);
		}
	} else {
		matrix_t my_out;
		matrix_slice_t my_A = *A;
		matrix_slice_t my_B = *B;
		matrix_t my_A_mat, my_B_mat;
		if (!out->gpu) {
			my_out = *out;
		} else {
			matrix_download(&my_out, out);
		}
		if (!A->base->gpu) {
			my_A = *A;
		} else {
			matrix_download(&my_A_mat, A);
			my_A.base = &my_A_mat;
		}
		if (!B->base->gpu) {
			my_B = *B;
		} else {
			matrix_download(&my_B_mat, B);
			my_B.base = &my_B_mat;
		}
		matrix_mult(&my_out, &my_A, &my_B);
		if (out->data != my_out.data) {
			matrix_slice_t my_out_slice = { &my_out, 0, 0, my_out.m, my_out.n };
			matrix_copy_internal(out, &my_out_slice, NULL, 1);
			matrix_free(&my_out);
		}
		if (A->base->gpu) {
			matrix_free(&my_A_mat);
		}
		if (B->base->gpu) {
			matrix_free(&my_B_mat);
		}
	}
}
void matrix_mult(matrix_t *out, matrix_slice_t *A, matrix_t *B) {
	matrix_slice_t B_slice = { B, 0, 0, B->m, B->n };
	matrix_mult(out, A, &B_slice);
}
void matrix_mult(matrix_t *out, matrix_t *A, matrix_slice_t *B) {
	matrix_slice_t A_slice = { A, 0, 0, A->m, A->n };
	matrix_mult(out, &A_slice, B);
}
void matrix_mult(matrix_t *out, matrix_t *A, matrix_t *B) {
	matrix_slice_t A_slice = { A, 0, 0, A->m, A->n };
	matrix_slice_t B_slice = { B, 0, 0, B->m, B->n };
	matrix_mult(out, &A_slice, &B_slice);
}

__global__ void matrix_trsyrk_tall_k1(float *out, float *A, int m, int n, char diag, int thrd_work) {
	long jtot = threadIdx.y + blockIdx.y*blockDim.y;
	int jl = jtot % n; // output row
	int jr = jtot/n + jl; // output column
	if (jl > n || jr > n || jl < jr) {
		return;
	}
	int i0 = threadIdx.x + thrd_work*blockIdx.x*blockDim.x;
	if (i0 < jl) {
		i0 = threadIdx.x + (jl/thrd_work)*thrd_work;
	}
	if (i0 < jr) {
		i0 = threadIdx.x + (jr/thrd_work)*thrd_work;
	}
	float acc = 0;
	if (i0 == jl && jl == jr) {
		if (diag == 'N') {
			acc = A[jl+jl*m] * A[jl+jl*m];
		} else if (diag == 'U') {
			acc = 1;
		}
		i0 += thrd_work;
	}
	for (int i=i0; i<m; i+=thrd_work) {
		acc += A[i+jl*m] * A[i+jr*m];
	}
	atomicAdd(out + jl + jr*n, acc);
	atomicAdd(out + jr + jl*n, acc);
}

void matrix_trsyrk(matrix_t *out, matrix_t *A, char diag, char uplo) {
	if (out->gpu && A->gpu) {
		assert(uplo == 'L');
		const int thrd_work = 16;
		dim3 B(DIV_UP(A->m,thrd_work), DIV_UP(A->n*A->n,16));
		dim3 T(4, 16);
		matrix_trsyrk_tall_k1(out->data, A->data, A->m, A->n, diag, thrd_work);
	} if (out->gpu) {
		assert(0);
	} else if (A->gpu) {
		assert(0);
	} else if (uplo == 'L') {
		for (int ir = 0; ir < A->n; ir++) {
			for (int il = 0; il <= ir; il++) {
				*matrix_index(out, il, ir) = 0;
				for (int j=MAX(il,ir); j<A->m; j++) {
					float acc = matrix_index_get(A, j, il) * matrix_index_get(A, j, ir);
					if (j == il && il == ir) {
						switch (diag) {
						case 'U':
							acc = 1;
						case 'Z':
							acc = 0;
						}
					}
					*matrix_index(out, il, ir) += acc;
				}
			}
		}
	} else {
		for (int ir = 0; ir < A->n; ir++) {
			for (int il = 0; il <= ir; il++) {
				*matrix_index(out, il, ir) = 0;
				for (int j=MIN(il,ir); j>=0; j--) {
					float acc = matrix_index_get(A, j, il) * matrix_index_get(A, j, ir);
					if (j == il && il == ir) {
						switch (diag) {
						case 'U':
							acc = 1;
						case 'Z':
							acc = 0;
						}
					}
					*matrix_index(out, il, ir) += acc;
				}
			}
		}
	}
}

void matrix_load(matrix_t *out, const char *filename) {
	// figure out format
	FILE *f = fopen(filename, "r");
	char buffer[1024];
	char pattern[1024];
	fgets(buffer, sizeof(buffer), f);
	// TODO: anything else
	assert(sscanf(buffer, "%%%%MatrixMarket matrix coordinate real %s", pattern) == 1);
	assert(strcmp(pattern, "general") == 0);

	// skip over matrix market comments
	int c;
	while ((c = fgetc(f)) == '%') {
		fgets(buffer, sizeof(buffer), f);
	}
	ungetc(c, f);
	long pos = ftell(f);

	// figure out matrix market dimensions
	int m = 0;
	int n = 0;
	while (!feof(f)) {
		fgets(buffer, sizeof(buffer), f);
		int i, j;
		float val;
		sscanf(buffer, " %d %d %f", &i, &j, &val);
		if (i > m) m = i;
		if (j > n) n = j;
	}
	out->data = NULL;
	out->m = m;
	out->n = n;
	out->colmaj = 1;
	out->gpu = 0;
	matrix_alloc(out);

	// actually read the matrix
	clearerr(f);
	fseek(f, pos, SEEK_SET);
	while (!feof(f)) {
		fgets(buffer, sizeof(buffer), f);
		int i, j;
		float val;
		sscanf(buffer, " %d %d %f", &i, &j, &val);
		*matrix_index(out, i-1, j-1) = val;
	}
	fclose(f);
}

void matrix_save(matrix_t *mat, const char *filename) {
	FILE *f = fopen(filename, "w");
	fprintf(f, "%%%%MatrixMarket matrix coordinate real general\n");
	for (int j=0; j<mat->n; j++) {
		for (int i=0; i<mat->m; i++) {
			if (*matrix_index(mat, i, j) != 0) {
				fprintf(f, "\t%d\t%d\t%e\n", i+1, j+1, *matrix_index(mat, i, j));
			}
		}
	}
	fclose(f);
}

__device__ int utils_bitset_get_c(unsigned long *bits, int idx) {
	unsigned long word = bits[idx / 64];
	return word & (1 << (idx % 64));
}

// this can most likely be optimised much more
__device__ float utils_pspsmepath_c(float *S, int n, unsigned long *iota, unsigned long *nu, int l, int r, void *work, float alpha) {
	// long and complicated setup code :(
	float base = alpha;
	int last = -1;
	for (int i=0; i<n; i++) {
		if (utils_bitset_get_c(iota, i)) { // TODO: reduce memory access
			if (last >= 0) {
				base *= S[last*n + i];
			}
			last = i;
		}
	}
/*	unsigned long *mask = work;
	for (int i=0; i<DIV_UP(n,64); i++) {
		int my_l = l - i*64;
		int my_r = r - i*64;
		unsigned long my_w = 0;
		if (my_l < 0 && my_r >= 0) {
			my_w -= 1;
			if (my_r < 64) {
				my_w += 1 << (my_r+1);
			}
		} else if (my_l < 64 && my_r >= 0) {
			my_w -= 1 << my_l;
			if (my_r < 64) {
				my_w += 1 << (my_r+1);
			}
		}
		mask[i] = my_w;
	} */
	int my_len = 0;
//	int *my_idxs = work + DIV_UP(n,64);
	int *my_idxs = work;
	for (int i=l; i<=r; i++) {
		if (!utils_bitset_get_c(iota,i) && !utils_bitset_get_c(nu,i)) {
			my_idxs[my_len] = i;
			my_len++;
		}
	}
	// actual computations :)
	unsigned long my_path = 3;
	float acc = 1;
	while (my_path < (1<<my_len)) {
		last = -1;
		for (int i=0; i<my_len; i++) {
			if (my_path & (1<<i)) {
				if (last >= 0) {
					acc *= S[last*n + my_idxs[i]];
				}
				last = my_idxs[i];
			}
		}
		my_path++;
	}
	return acc;
}

void matrix_apply_hh_inter(matrix_t *R, matrix_t *U, matrix_t *tau, matrix_t *y, char trans) {
	assert(R->m = U->m);
	assert(R->n = y->m);
	assert(U->n = tau->m);
	matrix_t S = { NULL, U->n, U->n, 1, 1 };
	matrix_alloc(&S);
	matrix_trsyrk(&S, U, 'L', 'U');
	const int IDX_TO_FIX = 8;
	dim3 B(U->n, 1);
	dim3 T(1, 1<<IDX_TO_FIX);
	matrix_apply_hh_inter_k1 <<< B, T >>> (R->data, U->data, tau->data, y->data, U->m, U->n, y->m, trans, IDX_TO_FIX);
}
