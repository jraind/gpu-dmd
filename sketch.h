#pragma once

#include "utils.h"

// fill the entries of an already allocated matrix
// with a Gaussian Embedding sketching
// accepts a version variable between 1 and 2 to select a method
//  * 1 uses cuRand
//  * 2 uses independent sums of 10 consecutive states of a change of variables of the logistic map at r=4
//  * 3 uses '' but more optimised
void matrix_sketch(matrix_t *out, int ver);
