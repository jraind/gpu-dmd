#!/bin/bash
#SBATCH -J gpu-dmd-self-time
#SBATCH --account=personal
#SBATCH --partition=a100_normal_q
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=1
#SBATCH --gres=gpu:1 --mem=16G
#SBATCH --time=30:00

date
echo "-----------------------------------"
module load cuda-latest/blas
module load cuda-latest/toolkit
module load openblas/dynamic
nvidia-smi
make clean
make
./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 10
#./demo
echo "-----------------------------------"
date
