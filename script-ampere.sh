#!/bin/bash

echo "-----------------------------------"
module load cuda-12.2
CFLAGS=
make clean
make
./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 1
# nvprof -o prof-ampere.out --metrics all ./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 1
echo "-----------------------------------"
