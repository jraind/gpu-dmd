#pragma once

#include "utils.h"

typedef struct dmd_opts_t_s {
	int rank; // expected rank of X
	int p; // optional amount to overestimate rank by
	int sketchver; // version parameter to pass to sketching
} dmd_opts_t;

// find the dynamic mode decomposition
// compare to Fl*Fr = X \ Y in MATLAB
// alg determines how to figure out the decomposition:
//  * 1 means find full SVD with gesvdp, then reduce properly by rtol
//  * 2 means find rank-rank randomised SVD with gesvdr with recommended accuracy, then reduce
//  * 3 means '' with gesvdr but only oversampling with 5 extra elements
//  * 4 means '' with sketch.h method opts->sketchver
// puts everything on the GPU to do the SVD there
// Fl and Fr are allocated by this routine on the GPU
void dmd(matrix_t *Fl, matrix_t *Fr, matrix_t *X, matrix_t *Y, float rtol, int alg, dmd_opts_t *opts);
