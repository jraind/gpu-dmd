#!/bin/bash

echo "-----------------------------------"
module load cuda-12.2
CFLAGS=-DPROFILE
make clean
make
TMPDIR=$HOME/tmp ncu -o prof-tka100.%i.out.ncu-rep --section WarpStateStats --section SourceCounters --section ComputeWorkloadAnalysis --section InstructionStats --section LaunchStats --section MemoryWorkloadAnalysis --section Occupancy --section SchedulerStats --section SpeedOfLight ./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 1
# nvprof -o prof-ampere.out --metrics all ./demo logistic-orbit-x.mtx logistic-orbit-y.mtx logistic-orbit-timings.mtx 1
echo "-----------------------------------"
