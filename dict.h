#pragma once

#include <cuda.h>
#include <math.h>
#include "matrix.h"
#include "utils.h"

typedef enum dict_enum_t {
	DICT_LAGUERRE_NOCROSS,
} dict_t;

class ImplicitLaguerreNocross : AbstractGpuMatrix {
private:
	int d, n;
	int ord;
	matrix_t base;
public:
	ImplicitLaguerreNocross(matrix_t *base_mat, int order);
	int getM() const;
	int getN() const;
	void toMatrix(matrix_t *out);
	void matmul(matrix_t *out, matrix_t *B, char side='L');
	void applyHHsequence(matrix_t *out, matrix_t *Q, char trans='N');
};

// evaluate the dictionary
// allocates out as column major with the proper size
// out is allocated on the same processor as in
// columns are mapped independently
void dict_eval(matrix_t *out, matrix_t *in, dict_t dict, int ord);
